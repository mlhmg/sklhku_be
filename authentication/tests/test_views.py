from .test_setup import TestSetUp
from ..models import User


class TestViews(TestSetUp):
    def test_user_cannot_register_with_no_data(self):
        res = self.client.post(self.register_url)
        self.assertEqual(res.status_code, 400)

    def test_sekolah_user_can_register_correctly(self):
        res = self.client.post(
            self.register_url, self.user_data, format="json")
        print(res)
        self.assertEqual(res.data["email"], self.user_data["email"])
        self.assertEqual(res.data["username"], self.user_data["username"])

        user = User.objects.get(email=res.data["email"])

        self.assertEqual(str(user), self.user_data["email"])
        self.assertTrue(user.is_sekolah)

        self.assertEqual(res.status_code, 201)
