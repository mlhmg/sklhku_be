from .test_setup import TestSetUp
from ..models import User
import unittest


class TestModels(unittest.TestCase):
    def setUp(self):
        self.email = "email@email.com"
        self.username = "username"
        self.password = "password"

    def test_if_user_not_send_username(self):
        with self.assertRaises(TypeError) as context:
            res = User.objects.create_user(None, self.email, self.password)
        self.assertTrue(
            'Users should have a username' in str(context.exception))

    def test_if_user_not_send_email(self):
        with self.assertRaises(TypeError) as context:
            res = User.objects.create_user(self.username, None, self.password)
        self.assertTrue(
            'Users should have an email' in str(context.exception))

    def test_create_super_user(self):
        res = User.objects.create_superuser(
            self.username, self.email, self.password)

        self.assertEqual(str(res), self.email)
        self.assertTrue(res.is_superuser)
        self.assertTrue(res.is_staff)
